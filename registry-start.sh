#!/bin/bash

# Starts the registry and builds listed docker images + pushes them to the registry

if [[  -z $CI_REGISTRY_IMAGE ]]; then
    $CI_REGISTRY_IMAGE=localhost:5000
fi

# check if registry is already running, if not spawn a registry instance
REGISTRY_RESPONSE=$(curl --write-out '%{http_code}' --silent --output /dev/null ${CI_REGISTRY_IMAGE})
if [[ ${REGISTRY_RESPONSE} != 200 ]]; then
    echo "Bringing up registry..."
    docker run -d -p 5000:5000 --restart=always --name registry registry:2
else
    echo "Registry is already running"
fi

# build images
docker build ./apps/hmo-solver --tag $CI_REGISTRY_IMAGE/hmo-solver:latest
docker build ./apps/hmo-django --tag $CI_REGISTRY_IMAGE/hmo-django:latest
docker build ./apps/frontend-hmo --tag $CI_REGISTRY_IMAGE/hmo-frontend:latest
docker build ./apps/front-nginx --tag $CI_REGISTRY_IMAGE/front-nginx:latest
docker build ./apps/frontend-bs --tag $CI_REGISTRY_IMAGE/frontend-bs:latest
docker build ./apps/frontend-nuxt --tag $CI_REGISTRY_IMAGE/frontend-nuxt:latest

# push images
docker push $CI_REGISTRY_IMAGE/hmo-solver:latest
docker push $CI_REGISTRY_IMAGE/hmo-django:latest
docker push $CI_REGISTRY_IMAGE/hmo-frontend:latest
docker push $CI_REGISTRY_IMAGE/front-nginx:latest
docker push $CI_REGISTRY_IMAGE/frontend-bs:latest
docker push $CI_REGISTRY_IMAGE/frontend-nuxt:latest
