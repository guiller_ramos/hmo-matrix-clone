#!/bin/bash

if [[ ${CI_REGISTRY_IMAGE} != "registry.gitlab.com/opswerks/hmo-matrix-collection" ]]; then
    echo "Skipping deploy because branch is not on opswerks/hmo-matrix-collection"
    exit 0
fi

export RELEASE_NAME="hmo-app-${DEPLOYMENT_ENVIRONMENT}"

echo "Deploying ${CI_REGISTRY_IMAGE} with release name: ${RELEASE_NAME}"

set +e
helm install ${RELEASE_NAME} helm/build/hmo-app
if [ $? -ne 0 ]; then
    set -e
    echo "helm install failed, likely already installed, attempting upgrade"
    helm upgrade ${RELEASE_NAME} helm/build/hmo-app
fi

echo "Deployment complete! Enjoy!"

exit 0
