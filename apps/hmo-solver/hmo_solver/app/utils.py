def validate_applicants_dict(content: dict):
    """
    Checks if given object matches stored schema
    Limitations: all list entries should have the same schema
    """
    data_struct = {
        "employee": {"name": str, "birthday": str, "employed": bool, "married": bool, "opswerksEmployee": bool},
        "dependents": [
            {
                "name": str,
                "birthday": str,
                "relationship": str,
                "employed": bool,
                "married": bool,
            }
        ],
    }

    try:
        validate_schema(content, data_struct)
    except AssertionError as e:
        print("Assertion Error: {}".format(e))
        return False
    return True


def validate_schema(obj, ref_obj):
    if isinstance(obj, dict):
        for key, value in obj.items():
            validate_schema(value, ref_obj[key])
    elif isinstance(obj, list):
        for child in obj:
            validate_schema(child, ref_obj[0])
    else:
        assert isinstance(obj, ref_obj)
