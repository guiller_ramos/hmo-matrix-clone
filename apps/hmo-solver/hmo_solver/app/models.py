from hmo_solver.app import db
from datetime import datetime

APPLICATION_STATES = {
    "pending": "PENDING",
    "processing": "PROCESSING",
    "approved": "APPROVED",
    "active": "ACTIVE",
}

APPLICANT_TYPES = {
    "primary": "PRIMARY",
    "dependent": "DEPENDENT",
}


class User(db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String, unique=True, nullable=False)
    email = db.Column(db.String, unique=True, nullable=False)
    applicant = db.relationship("Applicant", uselist=False, back_populates="user")


class Applicant(db.Model):
    __tablename__ = "applicant"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    birthday = db.Column(db.DateTime, nullable=False)
    relationship = db.Column(db.String, nullable=False, server_default="None")
    employed = db.Column(db.Boolean, nullable=False)
    married = db.Column(db.Boolean, nullable=False)
    opswerksEmployee = db.Column(db.Boolean, nullable=False, default=False)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=True)
    user = db.relationship("User", back_populates="applicant")

    applications = db.relationship("ApplicationApplicant", back_populates="applicant")

    @property
    def json(self):
        return {
            "name": self.name,
            "bday": self.birthday.strftime("%Y-%m-%d"),
            "relationship": self.relationship,
            "employed": self.employed,
            "married": self.married,
            "opswerksEmployee": self.opswerksEmployee,
        }


class Application(db.Model):
    __tablename__ = "application"
    id = db.Column(db.Integer, primary_key=True)
    plan_type = db.Column(db.String, nullable=False)

    application_applicants = db.relationship(
        "ApplicationApplicant", back_populates="application"
    )
    states = db.relationship("ApplicationState", back_populates="application")

    @property
    def state(self):
        return sorted(self.states, key=lambda x: x.date_created)[-1]

    @property
    def primary_applicant(self):
        return [
            application.applicant
            for application in self.application_applicants
            if application.applicant_type == APPLICANT_TYPES["primary"]
        ][0]

    @property
    def dependent_applicants(self):
        return [
            application.applicant
            for application in self.application_applicants
            if application.applicant_type == APPLICANT_TYPES["dependent"]
        ]

    @property
    def json(self):
        return {
            "plan_type": self.plan_type,
            "primary": self.primary_applicant.json,
            "dependents": [x.json for x in self.dependent_applicants],
            "state": self.state.state,
            "id": self.id,
        }

    @staticmethod
    def csv_headers():
        return [
            "Application ID",
            "Plan Type",
            "State",
            "Application Type",
            "Applicant",
            "Birthday",
            "Opswerks Employee",
            "Married",
            "Employed",
            "Relation to Opswerks Employee",
        ]

    def get_csv(self, with_headers=False):
        headers = Application.csv_headers()

        rows = []

        def build_row(applicant, application_type):
            data = {
                "Application ID": self.id,
                "Plan Type": self.plan_type,
                "State": self.state.state,
                "Application Type": application_type,
                "Applicant": applicant.name,
                "Birthday": applicant.birthday.strftime("%Y-%m-%d"),
                "Opswerks Employee": applicant.opswerksEmployee,
                "Married": applicant.married,
                "Employed": applicant.employed,
                "Relation to Opswerks Employee": applicant.relationship,
            }

            return list([data[x] for x in headers])

        # append primary
        rows = [
            build_row(self.primary_applicant, APPLICANT_TYPES["primary"]),
        ]

        if with_headers:
            rows = [headers] + rows

        # append dependents
        rows += [
            build_row(applicant, APPLICANT_TYPES["dependent"])
            for applicant in self.dependent_applicants
        ]
        return rows


class ApplicationApplicant(db.Model):
    __tablename__ = "application_applicant"
    application_id = db.Column(
        db.Integer, db.ForeignKey("application.id"), primary_key=True
    )
    applicant_id = db.Column(
        db.Integer, db.ForeignKey("applicant.id"), primary_key=True
    )
    date_created = db.Column(db.DateTime, default=datetime.now())
    applicant_type = db.Column(db.String, nullable=False)
    applicant = db.relationship("Applicant", back_populates="applications")
    application = db.relationship(
        "Application", back_populates="application_applicants"
    )


class ApplicationState(db.Model):
    __tablename__ = "application_state"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    date_created = db.Column(db.DateTime, default=datetime.now())

    application_id = db.Column(
        db.Integer, db.ForeignKey("application.id"), primary_key=True
    )

    state = db.Column(db.String, nullable=False)
    application = db.relationship("Application", back_populates="states")


db.create_all()
