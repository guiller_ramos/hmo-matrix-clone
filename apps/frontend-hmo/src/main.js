import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;

const NotFound = { template: "<p>Page not found</p>" };

const routes = {
  "/form": App,
};

const app = new Vue({
  data: {
    currentRoute: window.location.pathname,
  },
  computed: {
    ViewComponent() {
      return routes[this.currentRoute] || NotFound;
    },
  },
  render(h) {
    return h(this.ViewComponent);
  },
}).$mount("#app");

window.addEventListener("popstate", () => {
  app.currentRoute = window.location.pathname;
});
